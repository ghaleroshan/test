import React, { Component } from 'react';

class HomePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name:"",email:"",
      emailError:"",phone:"",dob:"",streetAddress:"",city:"",selectedState:"",selectedCountry:"",
      state:["select-state","NSW", "VIC", "QLD", "NT", "ACT", "SA", "TAS", "WA"],
      country:["select-country","Australia"],
      resultObj:{},
      results:[],
      viewResults:false,
      viewForm:true
    }
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(e)
  {
    
    this.setState({[e.target.name]:e.target.value}); //storing the filled value
    
    
  }
  validate =()=>{
    let emailError="";
    if(!this.state.email.includes("@")){
      emailError="invalid email";
    }
    if(emailError){
      this.setState({emailError});
      return false;
    }
    return true;
  };
  
  handleSubmit()
  {
    const isvalid=this.validate();
    const {name,email,phone,dob,streetAddress,city,selectedState,selectedCountry} = this.state;
    
     if(name !== "" && email !== "" && phone !== "" && dob !== "" && streetAddress !== "" && city !== "" && selectedState !== ""  && selectedCountry !== ""&& isvalid)
    {
      let obj = {name:name,email:email,phone:phone,dob:dob,streetAddress:streetAddress,selectedState:selectedState,selectedCountry:selectedCountry,city:city}
      this.props.parent.updateState(obj); //updating the value to results
    }
   
    else
    {
      alert("Please enter the details") //error message on empty fields
    }
    
  }
  render()
  {
    return (
      <div className="HomePage">
        <form>
        <div className="title">
          <h2>Fill the Records</h2>
        </div>
        <div className="info">
        <label >Name</label> <input type="text" name="name" value = {this.state.name}  onChange={(e) => { this.handleChange(e) }} ></input>
        <label>MobileNumber</label>  <input type="text" name="phone" value = {this.state.phone}  onChange={(e) => { this.handleChange(e) }}  ></input>
          <label>Email</label><input type="text" name="email" value = {this.state.email} onChange={(e) => { this.handleChange(e) }}   ></input>
    <div style={{fontsize:12,color:"red"}}>{this.state.emailError}</div>
         <label>DOB:</label> <input type="date" value = {this.state.dob}  onChange={(e) => { this.handleChange(e) }}  name="dob" ></input>
          <h2>Address:</h2>
          <input type="text" name="streetAddress" value = {this.state.streetAddress}  onChange={(e) => { this.handleChange(e) }} ></input>
        <label>City:</label>  <input type="text" name="city" value = {this.state.city}  onChange={(e) => { this.handleChange(e) }}  ></input>
          <select value={this.state.selectedState} onChange={(e) => this.handleChange(e) } name ="selectedState">
          {this.state.state.map((value) => <option key={value} value={value}>{value}</option>)}
          </select>
          <select value={this.state.selectedCountry} onChange={(e) => this.handleChange(e) } name ="selectedCountry">
          {this.state.country.map((value) => <option key={value} value={value}>{value}</option>)}
          </select>
        </div>
        <button type="button" href="/">View Results</button>
        <button type="button" href="/" onClick = {this.handleSubmit}>Submit</button>
      </form>
      </div>
    );
  }
}

export default HomePage;
